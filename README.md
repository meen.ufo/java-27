# JAVA-27
Scope variable

# Before correct this question
After clone this project please do these steps first
1. Use command below to create new branch with your name
    checkout -b {firstname}_{lastname}
    Example : checkout -b james_gosling

2. Use command below to push your branch to git
    git push origin {firstname}_{lastname}
    Example : git push origin james_gosling

# Database
1. Download and install (Skip this step if you already have docker)
    https://www.docker.com/products/docker-desktop

2. Pull image mysql using command below (Skip this step if you already have mysql image)
    docker pull mysql:latest

3. Open terminal and go to JAVA-27 folder

4. Stop docker database earlier using command below
    docker-compose down -v

5. Start docker database using command below
    docker-compose up -d

6. If you would like to stop docker database using command below
    docker-compose down -v

# Postman
https://www.getpostman.com/collections/4c0e437a752516f9e9b8

# Connect from database tool
MYSQL_DATABASE: prior_game_db
MYSQL_USER: dev
MYSQL_PASSWORD: dev


Edit here