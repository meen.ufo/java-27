package org.prior.game.scopevariable.service;

import org.prior.game.scopevariable.bean.DescriptionDetail;
import org.springframework.stereotype.Service;

@Service
public class DescriptionService {

    public DescriptionDetail getDescriptionDetail(int genderCode) {
        DescriptionDetail descriptionDetail = new DescriptionDetail();
        if( 1 == genderCode){
            descriptionDetail.setGenderCode(genderCode);
            descriptionDetail.setGenderDescription("MALE");
        }
        return descriptionDetail;
    }
}
