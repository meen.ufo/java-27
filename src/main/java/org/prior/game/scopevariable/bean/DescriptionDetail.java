package org.prior.game.scopevariable.bean;

import lombok.Data;

@Data
public class DescriptionDetail {
    private int genderCode;
    private String genderDescription;
}